﻿using System.Collections.Generic;

namespace mvcday1.ViewModel
{
    public class MyViewData
    {
        public string stringProp1 { get; set; }
        public int intProp2 { get; set; }
        public List<int> MyListInt {get; set; }

    }
}