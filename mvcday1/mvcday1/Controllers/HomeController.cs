﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using mvcday1.ViewModel;
using System.Drawing.Printing;
using System.IO;
using System.Drawing;

namespace mvcday1.Controllers
{
    public class HomeController : Controller
    {
        StreamReader streamToPrint;
        private Font printFont;

        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
            
            ViewBag.ToString();

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public String MioTest()
        {
            return "Questo è un esempio di come si faccia a ritornare una stringa sul browser";
        }

        public JsonResult JsonTest()
        {
            JsonResult js = new JsonResult();
            List<int> myListInt = new List<int>();
            myListInt.Add(1); myListInt.Add(2); myListInt.Add(3);
            js.Data = new MyViewData { stringProp1 = "stringa1", intProp2 = 12, MyListInt=myListInt };
            js.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return js;
        }

        public ContentResult ContentResultTest()
        {
            ContentResult cr = new ContentResult();
            cr.Content = "<html><body>html vuot vuoto</body></html>";
            return cr;
        }

        public string Print()
        {
            try
            {
                streamToPrint = new StreamReader
                   ("C:\\temp\\pippo.txt");
                try
                {
                    printFont = new Font("Arial", 10);
                    PrintDocument pd = new PrintDocument();
                    pd.PrinterSettings.PrinterName = "";
                    pd.PrinterSettings.PrinterName = "\\\\beppe-pc-win7\\cutepdf";
                    pd.PrintPage += new PrintPageEventHandler
                       (this.pd_PrintPage);
                    pd.PrinterSettings.PrintToFile = false;

                    pd.PrinterSettings.PrintFileName = "C:\\temp\\" + pd.DocumentName + ".pdf";
                    pd.OriginAtMargins = true;

                    pd.Print();
                }
                finally
                {
                    streamToPrint.Close();
                }
                return "Stampa effettuata correttamente";
            }
            catch (Exception ex)
            {
                string ritorno = "Stampa NON effettuata!\n Exception " + ex.StackTrace.ToString() + "\nMessaggio: " + ex.Message;
                return ritorno;
            }
            
        }

        public string PrintCutePDF()
        {
            //PrintDialog pDia = new PrintDialog();

            //PrinterSettings ps = new PrinterSettings();

            //pDia.Document = printDocumentMessageBoxTest;

            //pDia.Document.DocumentName = "Your filename here";



            //ps.PrinterName = "CutePDF Writer";

            //ps.PrintToFile = false;

            //ps.PrintFileName = "C:\\" + pDia.Document.DocumentName + ".pdf";



            //// take printer settings from the dialog and set into the PrintDocument object

            //pDia.Document.OriginAtMargins = true;

            //ps.DefaultPageSettings.Margins.Left = 2;

            //printDocumentMessageBoxTest.PrinterSettings = ps;



            //// start the printing process, catch exceptions

            //try
            //{

            //    printDocumentMessageBoxTest.Print();

            //}

            //catch (Exception exc)
            //{

            //    MessageBox.Show("Printing error!\n" + exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            //}

            return "vuoto";
        }

        // The PrintPage event is raised for each page to be printed. 
        private void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            string line = null;

            // Calculate the number of lines per page.
            linesPerPage = 10;

            // Print each line of the file. 
            while (count < linesPerPage &&
               ((line = streamToPrint.ReadLine()) != null))
            {
                yPos = topMargin + (count *
                   printFont.GetHeight(ev.Graphics));
                ev.Graphics.DrawString(line, printFont, Brushes.Black,
                   leftMargin, yPos, new StringFormat());
                count++;
            }

            // If more lines exist, print another page. 
            if (line != null)
                ev.HasMorePages = true;
            else
                ev.HasMorePages = false;
        }
    }
}
